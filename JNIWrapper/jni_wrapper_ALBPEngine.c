#include <stdio.h>
#include <stdlib.h>
#include <jni.h>
#include <string.h>

#include "../../unified_api/src/al/socket/al_socket.h"

//#define DEBUG

#define JNIALBPENGINE "it/unibo/dtn/JAL/JALEngine"
#define JNIALBPSOCKET "it/unibo/dtn/JAL/BPSocket"
#define JNIALBPBUNDLE "it/unibo/dtn/JAL/Bundle"

static void addNewJNIClass(const char* javaClassName, JNINativeMethod* funcArray, size_t size);
static void initJNIArray();
static void destroyJNIArray();

#define CHECKFIDNOTNULL(f) {if(f==NULL){return FALSE;}}
#define CHECKMIDNOTNULL(m) {if(m==NULL){return FALSE;}}
static boolean_t copy_bundle_to_java(JNIEnv *env, jobject ALBPBundle_, al_types_bundle_object bundle, al_types_bundle_payload_location payloadLocation);
static boolean_t copy_bundle_from_java(JNIEnv *env, jobject ALBPBundle_, al_types_bundle_object* bundle);

#ifdef DEBUG
static void debug_print_timestamp(al_types_creation_timestamp ts);
static void debug_print_bundle_object(al_types_bundle_object b);
#endif

// ***** START ALBPENGINE REGION *****
static jint c_init(JNIEnv *env, jobject obj)
{
	return al_socket_init();
}

static void c_destroy(JNIEnv *env, jobject obj) {
	al_socket_destroy();
}

/*static jchar c_get_eid_format(JNIEnv *env, jobject obj, jint URI_scheme)
{
	return al_socket_get_eid_format(URI_scheme);
}*/

static JNINativeMethod ALBPEngineJNIFuncs[] = {
	{ "c_init", "(CI)I", (void *)&c_init },
	{ "c_destroy", "()V", (void *)&c_destroy },
	//{ "c_get_eid_format", "()C", (void *)&c_get_eid_format }
};
// ***** END ALBPENGINE REGION *****

// ***** START ALBPSOCKET REGION *****
static jint c_unregister(JNIEnv *env, jobject obj, jint registrationDescriptor) {
	return al_socket_unregister(registrationDescriptor);
}

static jint c_register(JNIEnv *env, jobject obj, jobject ALBPSocket_, jstring dtnDemuxString, jint IPNDemux, jchar force_eid, jint ipn_node_forDTN2) {
	char* dtnDemux = NULL;
	al_socket_registration_descriptor RD;
	jclass ALBPSocket = (*env)->GetObjectClass(env, ALBPSocket_);
	
    dtnDemux = (char*) (*env)->GetStringUTFChars(env, dtnDemuxString , NULL);
	
	al_error result = al_socket_register(&RD, dtnDemux, IPNDemux, force_eid, ipn_node_forDTN2, "none", 0);
	
	// Debug
	#ifdef DEBUG
	printf("C: valore RD=%d\n", (int)RD);
	fflush(stdout);
	#endif
	
	jmethodID mid = (*env)->GetMethodID(env, ALBPSocket, "setRegistrationDescriptor_C", "(I)V");
	(*env)->CallVoidMethod(env, ALBPSocket_, mid, (jint)RD);

	return (jint) result;
}

#define CleanFree(p) {if (p!=NULL) {free(p);p=NULL;}}

static jint c_send(JNIEnv *env, jobject obj, jint registrationDescriptor, jobject ALBPBundle_) {
	al_types_bundle_object bundle_object;
	al_bundle_create(&bundle_object);
	
	// Convert bundle from Java to C format
	copy_bundle_from_java(env, ALBPBundle_, &bundle_object);
	
	al_error result = al_socket_send((int)registrationDescriptor, bundle_object, bundle_object.spec->destination, bundle_object.spec->report_to);
	
	if (result == AL_SUCCESS) {   // Set the creation timestamp back to Java
		jclass ALBPBundle = (*env)->GetObjectClass(env, ALBPBundle_);
		jmethodID mid;
		mid = (*env)->GetMethodID(env, ALBPBundle, "setCreationTimestamp_C", "(II)V");
		CHECKMIDNOTNULL(mid);
		(*env)->CallVoidMethod(env, ALBPBundle_, mid, (jint) bundle_object.spec->creation_ts.time, (jint) bundle_object.spec->creation_ts.seqno);
	}
	
	CleanFree(bundle_object.payload->buf.buf_val);
	CleanFree(bundle_object.payload->filename.filename_val);
	if (bundle_object.spec->extensions.extension_number > 0) {
		int i;
		for (i = 0; i < bundle_object.spec->extensions.extension_number; i++) {
			CleanFree(bundle_object.spec->extensions.extension_blocks[i].block_data.block_type_specific_data);
		}
		CleanFree(bundle_object.spec->extensions.extension_blocks);
		bundle_object.spec->extensions.extension_number = 0;
	}
	/*if (bundle_object.spec->blocks.blocks_len > 0) {
		int i;
		for (i = 0; i < bundle_object.spec->blocks.blocks_len; i++) {
			CleanFree(bundle_object.spec->blocks.blocks_val[i].block_data.data_val);
		}
		CleanFree(bundle_object.spec->blocks.blocks_val);
		bundle_object.spec->blocks.blocks_len = 0;
	}*/
	
	al_bundle_free(&bundle_object);
	return (jint) result;
}

#define ReadAndAssignIntFromMethodCalling(variable, methodName) { \
	jmethodID mid = (*env)->GetMethodID(env, ALBPBundle, methodName, "()I");\
	CHECKMIDNOTNULL(mid);\
	variable = (*env)->CallIntMethod(env, ALBPBundle_, mid);\
}

#define ReadAndAssignIntFromMethodCallingForMetadataAndBlocks(variable, value, methodName) { \
	jmethodID mid = (*env)->GetMethodID(env, ALBPBundle, methodName, "(I)I");\
	CHECKMIDNOTNULL(mid);\
	variable = (*env)->CallIntMethod(env, ALBPBundle_, mid, (jint) value);\
}

#define ReadAndAssignStringFromMethodCalling(variable, methodName) {\
	jmethodID mid = (*env)->GetMethodID(env, ALBPBundle, methodName, "()Ljava/lang/String;");\
	CHECKMIDNOTNULL(mid);\
	jstring jstr = (*env)->CallObjectMethod(env, ALBPBundle_, mid);\
	const char* str = (*env)->GetStringUTFChars(env, jstr, 0);\
	strcpy(variable, str);\
	(*env)->ReleaseStringUTFChars(env, jstr, str);\
}

static boolean_t copy_bundle_from_java(JNIEnv *env, jobject ALBPBundle_, al_types_bundle_object* bundle) {
	jclass ALBPBundle = (*env)->GetObjectClass(env, ALBPBundle_);
	int i;
	
	// Bundle ID
	ReadAndAssignStringFromMethodCalling(&(bundle->spec->source.uri[0]), "getSourceAsString");
	ReadAndAssignIntFromMethodCalling(bundle->spec->creation_ts.time, "getCreationTimestampSeconds");
	ReadAndAssignIntFromMethodCalling(bundle->spec->creation_ts.seqno, "getCreationTimestampSequenceNumber");
	
	// Bundle SPEC
	ReadAndAssignStringFromMethodCalling(&(bundle->spec->source.uri[0]), "getSourceAsString");
	ReadAndAssignStringFromMethodCalling(&(bundle->spec->destination.uri[0]), "getDestinationAsString");
	ReadAndAssignStringFromMethodCalling(&(bundle->spec->report_to.uri[0]), "getReplyToAsString");
	ReadAndAssignIntFromMethodCalling(bundle->spec->cardinal, "getPriorityVal");
	ReadAndAssignIntFromMethodCalling(bundle->spec->ecos.ordinal, "getPriorityOrdinal");
	ReadAndAssignIntFromMethodCalling(bundle->spec->bundle_proc_ctrl_flags, "getDeliveryOptionsVal");
	ReadAndAssignIntFromMethodCalling(bundle->spec->lifetime, "getExpiration");
	ReadAndAssignIntFromMethodCalling(bundle->spec->creation_ts.time, "getCreationTimestampSeconds");
	ReadAndAssignIntFromMethodCalling(bundle->spec->creation_ts.seqno, "getCreationTimestampSequenceNumber");
	ReadAndAssignIntFromMethodCalling(bundle->spec->delivery_regid, "getDeliveryRegID");
	ReadAndAssignIntFromMethodCalling(bundle->spec->ecos.unreliable, "getUnreliableAsVal");
	ReadAndAssignIntFromMethodCalling(bundle->spec->ecos.critical, "getCriticalAsVal");
	ReadAndAssignIntFromMethodCalling(bundle->spec->ecos.flow_label, "getFlowLabel");

	// Metadata
	ReadAndAssignIntFromMethodCalling(bundle->spec->extensions.extension_number, "getMetadataSize");
	if (bundle->spec->extensions.extension_number > 0)
		bundle->spec->extensions.extension_blocks = malloc(sizeof(al_types_extension_block) * bundle->spec->extensions.extension_number);
	for (i = 0; i < bundle->spec->extensions.extension_number; i++) {
		ReadAndAssignIntFromMethodCallingForMetadataAndBlocks(bundle->spec->extensions.extension_blocks[i].block_type_code, i, "getMetadataType");
		ReadAndAssignIntFromMethodCallingForMetadataAndBlocks(bundle->spec->extensions.extension_blocks[i].block_processing_control_flags, i, "getMetadataFlags");
		ReadAndAssignIntFromMethodCallingForMetadataAndBlocks(bundle->spec->extensions.extension_blocks[i].block_data.block_type_specific_data_len, i, "getMetadataDataSize");
		jmethodID mid = (*env)->GetMethodID(env, ALBPBundle, "getMetadataData", "(I)[B");
		CHECKMIDNOTNULL(mid);
		jbyteArray buffer = (*env)->CallObjectMethod(env, ALBPBundle_, mid, (jint) i);
		jbyte* bytes = (*env)->GetByteArrayElements(env, buffer, NULL);
		bundle->spec->extensions.extension_blocks[i].block_data.block_type_specific_data = malloc(bundle->spec->extensions.extension_blocks[i].block_data.block_type_specific_data_len);
		memcpy(bundle->spec->extensions.extension_blocks[i].block_data.block_type_specific_data, bytes, bundle->spec->extensions.extension_blocks[i].block_data.block_type_specific_data_len);
		(*env)->ReleaseByteArrayElements(env, buffer, bytes, JNI_ABORT);
	}
	
	// Blocks
	/*ReadAndAssignIntFromMethodCalling(bundle->spec->blocks.blocks_len, "getBlocksSize");
	if (bundle->spec->blocks.blocks_len > 0)
		bundle->spec->blocks.blocks_val = malloc(sizeof(al_types_extension_block) * bundle->spec->blocks.blocks_len);
	for (i = 0; i < bundle->spec->blocks.blocks_len; i++) {
		ReadAndAssignIntFromMethodCallingForMetadataAndBlocks(bundle->spec->blocks.blocks_val[i].block_type_code, i, "getBlocksType");
		ReadAndAssignIntFromMethodCallingForMetadataAndBlocks(bundle->spec->blocks.blocks_val[i].block_processing_control_flags, i, "getBlocksFlags");
		ReadAndAssignIntFromMethodCallingForMetadataAndBlocks(bundle->spec->blocks.blocks_val[i].block_data.data_len, i, "getBlocksDataSize");
		jmethodID mid = (*env)->GetMethodID(env, ALBPBundle, "getBlocksData", "(I)[B");
		CHECKMIDNOTNULL(mid);
		jbyteArray buffer = (*env)->CallObjectMethod(env, ALBPBundle_, mid, (jint) i);
		jbyte* bytes = (*env)->GetByteArrayElements(env, buffer, NULL);
		bundle->spec->blocks.blocks_val[i].block_data.data_val = malloc(bundle->spec->blocks.blocks_val[i].block_data.data_len);
		memcpy(bundle->spec->blocks.blocks_val[i].block_data.data_val, bytes, bundle->spec->blocks.blocks_val[i].block_data.data_len);
		(*env)->ReleaseByteArrayElements(env, buffer, bytes, JNI_ABORT);*/
	//}
	
	// Bundle PAYLOAD
	ReadAndAssignIntFromMethodCalling(bundle->payload->location, "getPayloadLocationAsVal");
	switch(bundle->payload->location) {
		case BP_PAYLOAD_FILE:
		case BP_PAYLOAD_TEMP_FILE:
			ReadAndAssignIntFromMethodCalling(bundle->payload->filename.filename_len, "getPayloadFileNameSize");
			bundle->payload->filename.filename_len++;
			bundle->payload->filename.filename_val = malloc(bundle->payload->filename.filename_len + 1);
			ReadAndAssignStringFromMethodCalling(bundle->payload->filename.filename_val, "getPayloadFileName");
			bundle->payload->filename.filename_val[bundle->payload->filename.filename_len] = '\0';
			break;
			
		case BP_PAYLOAD_MEM:
			ReadAndAssignIntFromMethodCalling(bundle->payload->buf.buf_len, "getPayloadMemorySize");
			jmethodID mid = (*env)->GetMethodID(env, ALBPBundle, "getPayloadMemoryData", "()[B");
			CHECKMIDNOTNULL(mid);
			jbyteArray buffer = (*env)->CallObjectMethod(env, ALBPBundle_, mid);
			jbyte* bytes = (*env)->GetByteArrayElements(env, buffer, NULL);
			bundle->payload->buf.buf_val = malloc(bundle->payload->buf.buf_len);
			memcpy(bundle->payload->buf.buf_val, bytes, bundle->payload->buf.buf_len);
			(*env)->ReleaseByteArrayElements(env, buffer, bytes, JNI_ABORT);
			
			break;
			
		default: return FALSE;
	}
	bundle->payload->status_report = NULL;
	
	#ifdef DEBUG
	debug_print_bundle_object(*bundle);
	#endif

	return TRUE;
}

static jint c_receive(JNIEnv *env, jobject obj, jint registrationDescriptor, jobject ALBPBundle_, jint payloadLocation, jint timeout) {
	al_types_bundle_object bundle_object;
	al_bundle_create(&bundle_object);
	
	al_error result = al_socket_receive((al_socket_registration_descriptor) registrationDescriptor, &bundle_object, (al_types_bundle_payload_location) payloadLocation, (al_types_timeval) timeout);
	
	#ifdef DEBUG
	printf("Exited from al_socket_receive.\n\tResult=%d\n", result);
	if (result != 12)
		debug_print_bundle_object(bundle_object);
	fflush(stdout);
	#endif
	
	if (result == AL_SUCCESS) {
		boolean_t copied = copy_bundle_to_java(env, ALBPBundle_, bundle_object, (al_types_bundle_payload_location) payloadLocation);

		if (!copied) {
			#ifdef DEBUG
			printf("Error on copying bundle to Java.\n");
			fflush(stdout);
			#endif
			result = AL_WARNING_RECEPINTER;
		}

		if (payloadLocation == BP_PAYLOAD_FILE || payloadLocation == BP_PAYLOAD_TEMP_FILE) {
			bundle_object.payload->filename.filename_val = NULL; // Avoid the deletion of the bundle file (removing file). Java will do it.
		}
	}
	
	al_bundle_free(&bundle_object);
	
	return (jint) result;
}

static boolean_t copy_bundle_to_java(JNIEnv *env, jobject ALBPBundle_, al_types_bundle_object bundle, al_types_bundle_payload_location payloadLocation) {
	jclass ALBPBundle = (*env)->GetObjectClass(env, ALBPBundle_);
	jmethodID mid;
	jbyteArray buffer;
	int i;
	
	mid = (*env)->GetMethodID(env, ALBPBundle, "setSource_C", "(Ljava/lang/String;)V");
	CHECKMIDNOTNULL(mid);
	(*env)->CallVoidMethod(env, ALBPBundle_, mid, (*env)->NewStringUTF(env, bundle.spec->source.uri));
	
	mid = (*env)->GetMethodID(env, ALBPBundle, "setCreationTimestamp_C", "(II)V");
	CHECKMIDNOTNULL(mid);
	(*env)->CallVoidMethod(env, ALBPBundle_, mid, (jint) bundle.spec->creation_ts.time, (jint) bundle.spec->creation_ts.seqno);
	
	/*mid = (*env)->GetMethodID(env, ALBPBundle, "setFragmentOffset_C", "(I)V");
	CHECKMIDNOTNULL(mid);
	(*env)->CallVoidMethod(env, ALBPBundle_, mid, (jint) bundle.id->frag_offset);
	
	mid = (*env)->GetMethodID(env, ALBPBundle, "setOrigLength_C", "(I)V");
	CHECKMIDNOTNULL(mid);
	(*env)->CallVoidMethod(env, ALBPBundle_, mid, (jint) bundle.id->frag_length);*/
	
	mid = (*env)->GetMethodID(env, ALBPBundle, "setDestination_C", "(Ljava/lang/String;)V");
	CHECKMIDNOTNULL(mid);
	(*env)->CallVoidMethod(env, ALBPBundle_, mid, (*env)->NewStringUTF(env, bundle.spec->destination.uri));
	
	mid = (*env)->GetMethodID(env, ALBPBundle, "setReplyTo_C", "(Ljava/lang/String;)V");
	CHECKMIDNOTNULL(mid);
	(*env)->CallVoidMethod(env, ALBPBundle_, mid, (*env)->NewStringUTF(env, bundle.spec->report_to.uri));
	
	mid = (*env)->GetMethodID(env, ALBPBundle, "setPriority_C", "(II)V");
	CHECKMIDNOTNULL(mid);
	(*env)->CallVoidMethod(env, ALBPBundle_, mid, (jint) bundle.spec->cardinal, (jint) bundle.spec->ecos.ordinal);
	
	mid = (*env)->GetMethodID(env, ALBPBundle, "setDeliveryOption_C", "(I)V");
	CHECKMIDNOTNULL(mid);
	(*env)->CallVoidMethod(env, ALBPBundle_, mid, (jint) bundle.spec->delivery_regid);
	
	mid = (*env)->GetMethodID(env, ALBPBundle, "setExpiration_C", "(I)V");
	CHECKMIDNOTNULL(mid);
	(*env)->CallVoidMethod(env, ALBPBundle_, mid, (jint) bundle.spec->lifetime);
	
	mid = (*env)->GetMethodID(env, ALBPBundle, "setDeliveryRegID_C", "(I)V");
	CHECKMIDNOTNULL(mid);
	(*env)->CallVoidMethod(env, ALBPBundle_, mid, (jint) bundle.spec->delivery_regid);
	
	// METADATA
	mid = (*env)->GetMethodID(env, ALBPBundle, "addMetadata_C", "([BII)V");
	CHECKMIDNOTNULL(mid);
	for(i = 0; i < bundle.spec->extensions.extension_number; i++) {
		buffer = (*env)->NewByteArray(env, (jint) bundle.spec->extensions.extension_blocks->block_data.block_type_specific_data_len);
		(*env)->SetByteArrayRegion(env, buffer, 0, (jint) bundle.spec->extensions.extension_blocks->block_data.block_type_specific_data_len, (jbyte*) bundle.spec->extensions.extension_blocks->block_data.block_type_specific_data);
		(*env)->CallVoidMethod(env, ALBPBundle_, mid, buffer, (jint) bundle.spec->extensions.extension_blocks->block_processing_control_flags, bundle.spec->extensions.extension_blocks->block_type_code);
	}
	
	// BLOCKS
	/*mid = (*env)->GetMethodID(env, ALBPBundle, "addBlock_C", "([BII)V");
	CHECKMIDNOTNULL(mid);
	for(i = 0; i < bundle.spec->blocks.blocks_len; i++) {
		buffer = (*env)->NewByteArray(env, (jint) bundle.spec->blocks.blocks_val->block_data.data_len);
		(*env)->SetByteArrayRegion(env, buffer, 0, (jint) bundle.spec->blocks.blocks_val->block_data.data_len, (jbyte*) bundle.spec->blocks.blocks_val->block_data.data_val);
		(*env)->CallVoidMethod(env, ALBPBundle_, mid, buffer, (jint) bundle.spec->blocks.blocks_val->block_processing_control_flags, bundle.spec->blocks.blocks_val->block_type_code);	
	}*/
	
	mid = (*env)->GetMethodID(env, ALBPBundle, "setUnreliable_C", "(I)V");
	CHECKMIDNOTNULL(mid);
	(*env)->CallVoidMethod(env, ALBPBundle_, mid, (jint) bundle.spec->ecos.unreliable);
	
	mid = (*env)->GetMethodID(env, ALBPBundle, "setCritical_C", "(I)V");
	CHECKMIDNOTNULL(mid);
	(*env)->CallVoidMethod(env, ALBPBundle_, mid, (jint) bundle.spec->ecos.critical);
	
	mid = (*env)->GetMethodID(env, ALBPBundle, "setFlowLabel_C", "(I)V");
	CHECKMIDNOTNULL(mid);
	(*env)->CallVoidMethod(env, ALBPBundle_, mid, (jint) bundle.spec->ecos.flow_label);
	
	// PAYLOAD
	switch (payloadLocation) {
		case BP_PAYLOAD_MEM:
			mid = (*env)->GetMethodID(env, ALBPBundle, "setBuffer_C", "([B)V");
			CHECKMIDNOTNULL(mid);
			buffer = (*env)->NewByteArray(env, (jint) bundle.payload->buf.buf_len);
			(*env)->SetByteArrayRegion(env, buffer, 0, (jint) bundle.payload->buf.buf_len, (jbyte*) bundle.payload->buf.buf_val);
			(*env)->CallVoidMethod(env, ALBPBundle_, mid, buffer);
			break;
		
		case BP_PAYLOAD_FILE:
			mid = (*env)->GetMethodID(env, ALBPBundle, "setPayloadFile_C", "(Ljava/lang/String;)V");
			CHECKMIDNOTNULL(mid);
			(*env)->CallVoidMethod(env, ALBPBundle_, mid, (*env)->NewStringUTF(env, bundle.payload->filename.filename_val));
			break;
			
		case BP_PAYLOAD_TEMP_FILE:
			mid = (*env)->GetMethodID(env, ALBPBundle, "setPayloadTemporaryFile_C", "(Ljava/lang/String;)V");
			CHECKMIDNOTNULL(mid);
			(*env)->CallVoidMethod(env, ALBPBundle_, mid, (*env)->NewStringUTF(env, bundle.payload->filename.filename_val));
			break;
		
		default:
		break;
	}
	
	if (bundle.payload->status_report != NULL) {
			// BUNDLE PAYLOAD -> STATUS REPORT
			mid = (*env)->GetMethodID(env, ALBPBundle, "setSourceStatusReport_C", "(Ljava/lang/String;)V");
			CHECKMIDNOTNULL(mid);
			(*env)->CallVoidMethod(env, ALBPBundle_, mid, (*env)->NewStringUTF(env, bundle.payload->status_report->bundle_id.source.uri));
			
			mid = (*env)->GetMethodID(env, ALBPBundle, "setCreationTimestampStatusReport_C", "(II)V");
			CHECKMIDNOTNULL(mid);
			(*env)->CallVoidMethod(env, ALBPBundle_, mid, bundle.payload->status_report->bundle_id.creation_ts.time, bundle.payload->status_report->bundle_id.creation_ts.seqno);
			
			mid = (*env)->GetMethodID(env, ALBPBundle, "setFragmentOffsetStatusReport_C", "(I)V");
			CHECKMIDNOTNULL(mid);
			(*env)->CallVoidMethod(env, ALBPBundle_, mid, bundle.payload->status_report->bundle_id.frag_offset);
			
			mid = (*env)->GetMethodID(env, ALBPBundle, "setOrigLengthStatusReport_C", "(I)V");
			CHECKMIDNOTNULL(mid);
			(*env)->CallVoidMethod(env, ALBPBundle_, mid, bundle.payload->status_report->bundle_id.frag_length);
			
			mid = (*env)->GetMethodID(env, ALBPBundle, "setReasonStatusReport_C", "(I)V");
			CHECKMIDNOTNULL(mid);
			(*env)->CallVoidMethod(env, ALBPBundle_, mid, bundle.payload->status_report->reason);
			
			mid = (*env)->GetMethodID(env, ALBPBundle, "setFlagStatusReport_C", "(I)V");
			CHECKMIDNOTNULL(mid);
			(*env)->CallVoidMethod(env, ALBPBundle_, mid, bundle.payload->status_report->flags);
			
			mid = (*env)->GetMethodID(env, ALBPBundle, "setReceiptTimestampStatusReport_C", "(II)V");
			CHECKMIDNOTNULL(mid);
			(*env)->CallVoidMethod(env, ALBPBundle_, mid, bundle.payload->status_report->reception_ts, bundle.payload->status_report->reception_ts);
			
			mid = (*env)->GetMethodID(env, ALBPBundle, "setCustodyTimestampStatusReport_C", "(II)V");
			CHECKMIDNOTNULL(mid);
			(*env)->CallVoidMethod(env, ALBPBundle_, mid, bundle.payload->status_report->custody_ts, bundle.payload->status_report->custody_ts);
			
			mid = (*env)->GetMethodID(env, ALBPBundle, "setForwardingTimestampStatusReport_C", "(II)V");
			CHECKMIDNOTNULL(mid);
			(*env)->CallVoidMethod(env, ALBPBundle_, mid, bundle.payload->status_report->forwarding_ts, bundle.payload->status_report->forwarding_ts);
			
			mid = (*env)->GetMethodID(env, ALBPBundle, "setDeliveryTimestampStatusReport_C", "(II)V");
			CHECKMIDNOTNULL(mid);
			(*env)->CallVoidMethod(env, ALBPBundle_, mid, bundle.payload->status_report->delivery_ts, bundle.payload->status_report->delivery_ts);
			
			mid = (*env)->GetMethodID(env, ALBPBundle, "setDeletionTimestampStatusReport_C", "(II)V");
			CHECKMIDNOTNULL(mid);
			(*env)->CallVoidMethod(env, ALBPBundle_, mid, bundle.payload->status_report->deletion_ts, bundle.payload->status_report->deletion_ts);
			
			mid = (*env)->GetMethodID(env, ALBPBundle, "setAckByAppTimestampStatusReport_C", "(II)V");
			CHECKMIDNOTNULL(mid);
			(*env)->CallVoidMethod(env, ALBPBundle_, mid, bundle.payload->status_report->ack_by_app_ts, bundle.payload->status_report->ack_by_app_ts);
		}
	
	return TRUE;
}

#ifdef DEBUG
static void debug_print_timestamp(al_types_creation_timestamp ts) {
	printf("%d.%d", ts.time,ts.seqno);
}

static void debug_print_bundle_object(al_types_bundle_object b) {
	// BUNDLE ID
	printf("\tid=%p\n",b.id);
	if (b.id != NULL) {
		printf("\t\tsource=%s\n",b.id->source.uri);
		printf("\t\tcreation_ts=%d.%d\n",b.id->creation_ts.time, b.id->creation_ts.seqno);
		printf("\t\tfrag_offset=%d\n",b.id->frag_offset);
		printf("\t\torig_length=%d\n",b.id->frag_length);
	} else {
		printf("There are no b.id.\n");
	}
	
	// BUNDLE SPEC
	printf("\tspec=%p\n",b.spec);
	if (b.spec != NULL) {
		printf("\t\tsource=%s\n", b.spec->source.uri);
		printf("\t\tdest=%s\n", b.spec->destination.uri);
		printf("\t\treplyto=%s\n", b.spec->report_to.uri);
		printf("\t\tpriority=%d.%d\n", b.spec->cardinal_priority.cardinal_priority, b.spec->cardinal_priority.ordinal);
		printf("\t\tdopts=%d\n", b.spec->bundle_proc_ctrl_flags);
		printf("\t\texpiration=%d\n", b.spec->lifetime);
		printf("\t\tcreation_ts=%d.%d\n", b.spec->creation_ts.time, b.spec->creation_ts.seqno);
		printf("\t\tdelivery_regid=%d\n", b.spec->delivery_regid);
		printf("\t\tblocks len=%d\n", b.spec->blocks.blocks_len);
		printf("\t\tmetadata len=%d\n", b.spec->metadata.metadata_len);
		printf("\t\tunreliable=%d\n", (int)b.spec->unreliable);
		printf("\t\tcritical=%d\n", (int)b.spec->critical);
		printf("\t\tflow_label=%d\n", b.spec->flow_label);
	} else {
		printf("There are no b.spec.\n");
	}
	
	// BUNDLE PAYLOAD
	printf("\tpayload=%p\n", b.payload);
	if (b.payload != NULL) {
		printf("\t\tpayloadLocation=%d\n", b.payload->location);
		printf("\t\tfilename:\n\t\t\tlen=%d\n\t\t\tval=%s\n", b.payload->filename.filename_len, b.payload->filename.filename_val);
		printf("\t\tbuf:\n\t\t\tlen=%d\n\t\t\tval=%s\n", b.payload->buf.buf_len, b.payload->buf.buf_val);
		printf("\t\tstatus_report=%p\n", b.payload->status_report);
		
		if (b.payload->status_report != NULL) {
			// BUNDLE PAYLOAD -> STATUS REPORT
			printf("\t\t\t\tsource=%s\n", b.payload->status_report->bundle_id.source.uri);
			printf("\t\t\t\tcreation_ts=%d.%d\n",b.payload->status_report->bundle_id.creation_ts.time, b.payload->status_report->bundle_id.creation_ts.seqno);
			printf("\t\t\t\tfrag_offset=%d\n",b.payload->status_report->bundle_id.frag_offset);
			printf("\t\t\t\torig_length=%d\n",b.payload->status_report->bundle_id.frag_length);
			printf("\t\t\treason=%d\n", b.payload->status_report->reason);
			printf("\t\t\tflags=%d\n", b.payload->status_report->block_processing_control_flags);
	
			printf("\t\t\treception_ts=");
			debug_print_timestamp(b.payload->status_report->reception_ts);
			printf("\n");
			printf("\t\t\tcustody_ts=");
			debug_print_timestamp(b.payload->status_report->custody_ts);
			printf("\n");
			printf("\t\t\tforwarding_ts=");
			debug_print_timestamp(b.payload->status_report->forwarding_ts);
			printf("\n");
			printf("\t\t\tdelivery_ts=");
			debug_print_timestamp(b.payload->status_report->delivery_ts);
			printf("\n");
			printf("\t\t\tdeletion_ts=");
			debug_print_timestamp(b.payload->status_report->deletion_ts);
			printf("\n");
			printf("\t\t\tack_by_app_ts=");
			debug_print_timestamp(b.payload->status_report->ack_by_app_ts);
			printf("\n");
		} else {
		printf("There are no b.payload->status_report.\n");
	}
	}  else {
		printf("There are no b.payload.\n");
	}
}
#endif

static jstring c_get_local_eid (JNIEnv *env, jobject obj, jint registrationDescriptor) {
	al_types_endpoint_id local_eid = al_socket_get_local_eid((al_socket_registration_descriptor) registrationDescriptor);
	return (*env)->NewStringUTF(env, &(local_eid.uri[0]));
}

static JNINativeMethod ALBPSocketJNIFuncs[] = {
	{ "c_register", "(L"JNIALBPSOCKET";Ljava/lang/String;I)I", (void *)&c_register },
	{ "c_unregister", "(I)I", (void *)&c_unregister },
	{ "c_receive", "(IL"JNIALBPBUNDLE";II)I", (void*) &c_receive },
	{ "c_get_local_eid", "(I)Ljava/lang/String;", (void*) &c_get_local_eid},
	{ "c_send", "(IL"JNIALBPBUNDLE";)I", (void*) &c_send }
};
// ***** END ALBPSOCKET REGION *****

typedef struct {
	const char* className;
	JNINativeMethod* funcs;
	size_t size;
} ALBPJNIWrapperLinker;

static int albpJNIWrapperLinkerDim = 2;
static ALBPJNIWrapperLinker* albpJNIWrapperLinker = NULL;

static void addNewJNIClass(const char* javaClassName, JNINativeMethod* funcArray, size_t size) {
	static int arrayPosition = 0;
	if (arrayPosition >= albpJNIWrapperLinkerDim)
		return;
	albpJNIWrapperLinker[arrayPosition].className = javaClassName;
	albpJNIWrapperLinker[arrayPosition].funcs = funcArray;
	albpJNIWrapperLinker[arrayPosition].size = size / sizeof(JNINativeMethod);
	arrayPosition++;
}

static void destroyJNIArray() {
	free(albpJNIWrapperLinker);
}

JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* vm, void* reserved)
{
	JNIEnv *env;
	int i;

	(void)reserved;

	if ((*vm)->GetEnv(vm, (void **)&env, JNI_VERSION_1_6) != JNI_OK)
		return -1;
	
	initJNIArray();
	
	// Registers natives for each class
	for (i = 0; i < albpJNIWrapperLinkerDim; i++) {
		jclass  cls;
		jint    res;
		ALBPJNIWrapperLinker current = albpJNIWrapperLinker[i];
		
		#ifdef DEBUG
		printf("Linking class: %s\n", current.className);
		#endif
		
		cls = (*env)->FindClass(env, current.className);
		if (cls == NULL) {
			return -1;
		}

		res = (*env)->RegisterNatives(env, cls, current.funcs, current.size);
		if (res != 0) {
			return -1;
		}
	}
	
	

	return JNI_VERSION_1_6;
}

JNIEXPORT void JNICALL JNI_OnUnload(JavaVM *vm, void *reserved)
{
	JNIEnv *env;
	int i;
	
	(void)reserved;

	if ((*vm)->GetEnv(vm, (void **)&env, JNI_VERSION_1_6) != JNI_OK)
		return;

	for (i = 0; i < albpJNIWrapperLinkerDim; i++) {
		jclass  cls;
		ALBPJNIWrapperLinker current = albpJNIWrapperLinker[i];
		
		cls = (*env)->FindClass(env, current.className);
		if (cls == NULL)
			return;

		(*env)->UnregisterNatives(env, cls);
	}
	
	destroyJNIArray();
	
	al_socket_destroy(); // To be sure to do it
}

// EDIT THIS FUNCTION TO ADD NEW FUNCTIONALITIES
static void initJNIArray() {
	albpJNIWrapperLinker = calloc(albpJNIWrapperLinkerDim, sizeof(ALBPJNIWrapperLinker));
	
	addNewJNIClass(JNIALBPENGINE, &(ALBPEngineJNIFuncs[0]), sizeof(ALBPEngineJNIFuncs));
	addNewJNIClass(JNIALBPSOCKET, &(ALBPSocketJNIFuncs[0]), sizeof(ALBPSocketJNIFuncs));
}
